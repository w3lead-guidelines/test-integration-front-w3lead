# Test d'intégration W3lead

Montre nous tes connaissances en intégration et en framework JS avec ce test d'intégration sur Gatsby !

## 🚀 Pour commencer

1.  **Commencer à développer**

    Rends-toi dans le répertoire du site et commence le développement !

    ```shell
    cd test-integration-front/
    yarn install
    gatsby develop
    ```
    
    Tu peux aussi utiliser la commande ```npm install``` à la place de Yarn si tu préfères. 

1.  **Voir ton travail**

    Le projet sera ensuite accessible à l'adresse suivante : `http://localhost:8000`!

## 🤓 Quelques consignes

1. Au touch, le style et l'état des liens de la barre de navigation change comme indiqué sur la maquette
1. Au touch, le style et l’état des ronds de couleurs change comme indiqué sur la maquette
1. Dans le détail de la fiche produit, je peux modifier la quantité de 1 à 10
1. Au touch sur «Add to Cart», afficher un message dans la console js en gras de couleur verte «Produit ajouté au panier» 

## 🤫 Quelques astuces

1. Utilise [gatsby-plugin-react-svg](https://www.gatsbyjs.org/packages/gatsby-plugin-react-svg/) pour intégrer du contenu svg
1. Pour les transitions de tes pages, [rends-toi ici](https://www.gatsbyjs.org/docs/adding-page-transitions-with-plugin-transition-link/).

## 🧠 Pour aller plus loin

1. Déployer l'application sur netlify
2. Ajouter un effet de transition entre les deux pages
3. Lier complètement le contenu de la page "Détails" avec la miniature en mettant a jour dynamiquement le nom du produit, l'image et le titre.

## 🤔 Une question ? 

Tu peux m'écrire à l'adresse suivante : [thiebauld@w3lead.com](mailto:thiebauld@w3lead.com)